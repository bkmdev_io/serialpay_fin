

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>CP | Account Information</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="../../dist/css/AdminLTE.css">
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
     <header class="main-header">
          <nav class="navbar navbar-static-top" role="navigation">
          <div class="navbar-custom-menu">
               <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                         <ul class="dropdown-menu">
                             <li class="user-footer">
                             <div class="pull-right">
                                <a href="../../logout.php" class="btn btn-default btn-flat">Log out</a>
                             </div>
                             </li>
                         </ul>
                    </li>
               </ul>
          </div>
          </nav>
     </header>
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
   <h1>
   <a href="department.php">Transaction Information</a> / 
    Account Information
    </h1>
</section>
<!-- Main content -->
<section class="content">
 <div class="row">
   <div class="col-md-4">
     <!-- Profile Image -->
     <div class="box box-primary">
       <div class="box-body box-profile">
	   <img class="profile-user-img img-responsive img-circle" src="../../dist/img/enterprise-icon.png" alt="User profile picture">

              <h3 class="profile-username text-center">Department Store</h3>
         
</div>
</div>
</div>

        <div class="col-md-6">
          <div class="box">
            <div class="box-header">
              <h2 class="box-title">12-Digit Serial Number</h3>
            </div><br>
			
			<form action="phpScripts/serialProcess.php" method="POST" class="form-horizontal">
              <div class="box-body">                      
              <div class="input-group">
                <span class="input-group-addon">Serial Number</span>
                <input type="number" required name="serial" class="form-control" placeholder="XXXXXXXXXXXX">
              </div>              
              <!-- /.box-body -->
              <div class="box-footer">
                <br>
                <button type="submit" class="btn btn-md btn-success btn-block pull-right">Submit</button>
              </div>
              <!-- /.box-footer -->
              </div>
            </form>
            

          </div>
		  </div>
		  
<!-- 		  <div class="col-md-4">
          <div class="box">
            <div class="box-header">
              <h2 class="box-title">QR Code</h3>
            </div>
           
            
			
          </div>
		  </div> -->
		  
<script src="../../plugins/jQuery/jQuery-2.2.0.min.js"></script>
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<script src="../../plugins/fastclick/fastclick.js"></script>
<script src="../../dist/js/app.min.js"></script>
<script src="../../dist/js/demo.js"></script>
</body>
</html>
